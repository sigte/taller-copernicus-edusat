# Annex: guia d'ús de l’EO Browser de Sentinel Hub

###### 
https://www.sentinel-hub.com/explore/eobrowser

## Search

   - **1.** Search for the location of your interest either by scrolling the map or enter the location in the search field on the right side of the screen
   - **2.** Choose from which satellites you want to receive the data
   - **3.** Select maximum cloud coverage
   - **4.** Select time and range
   - **5.** You can also use predefined Theme (Wildfires, Volcanoes..)
   
![1](_static/EO_browser1.jpg)

## Results

- next to the thumbnail of your location - image information is shown"
    - sensing date
    - sensing time
    - cloud coverage
    - tile crs
    - mgrs location
- clicking on the button **Visualize** will open the image in visualisation tab

![2](_static/EO_browser2.jpg)

## Visualization

You can apply different pre-installed or custom spectral band combinations to visualise images

**Pre installed** are: 
   - True colour - visual interpretation of land cover
   - False colour - visual interpretation of vegetation
   - NDVI - vegetation index
   - Moisture index
   - SWIR - shortwave-infrared index    
   - NDWI - Normalized Difference Water Index
   - NDSI - Normalized Difference Snow Index
              
**Custom script** - you can select custom band combinations or write own classification script for visualisation of images

![3](_static/EO_browser3.jpg)

On the right side of the window, under ‘search’ bar you can find tools for drawing your **area of interest** or **point of interest**. Also **measuring tool** and buttons for **downloading** image in GeoTiff, JPG or KMZ format or in analytical panel you can select multiple layers and download them in zip file. The last button stands for **timelapse** where you can create chronological animation of the location, as well you can select the images manually by the date or cloud coverage.

![4](_static/EO_browser4.jpg)

For saving current result/s click on the **pin** button

## Pins

In this section you can find your pinned images and clicking on the **compare** button will compare pinned images in 2 modes:
  - Opacity - you can draw the slider left or right to fade between compared images
  - Split - you can draw the slider left or right to set the boundary between compared images
  
![5](_static/EO_browser5.jpg)
