# Casos d'ús

## DESASTRES NATURALS I DEGRADACIÓ ANTRÒPICA ##
Sovint els desastres naturals i les accions antròpiques deixen unes seqüeles sobre el territori que són visibles des de l’espai exterior i són, per tant, captades pels satèl·lits que orbiten al voltant de la Terra. Entre aquests fenòmens hi ha incendis, inundacions, sequeres, desglaç, desforestació i creixement d’estructures urbanes. A continuació es mostra com, mitjançant la combinació de bandes, es pot detectar el moment just en què es produeixen alguns esdeveniments. Aquests casos que s’han escollit exemplifiquen gràficament les conseqüències territorials ja que s’observa amb claredat l’abans i el després dels successos.

* [1. Incendis forestals](#incendis-forestals)
* [2. Retroces de glacials](#retroces-glacials)
* [3. Inundacions](#inundacions)
* [4. Erupcions volcaniques](#erupcions-volcaniques)
* [5. Desforestacio](#desforestacio)
* [6. Sequeres](#sequeres)
* [7. Urbanitzacio](#urbanitzacio)

# Incendis forestals

Els incendis forestals solen tenir lloc durant els mesos de més calor, justament coincidint amb els moments en què les masses forestals (boscos i brolles) estan seques. És aleshores quan una petita guspira pot aixecar un gran foc que pot quedar fora de control si les masses forestals formen un continu i, més encara, si les condicions atmosfèriques (manca de pluges i presència de vent) n’afavoreixen la seva propagació.

## Foc de Kineta i de Nea Makri, Grècia ##

Això mateix és el que va passar a prop d’Atenes, la capital de Grècia, el 23 juliol de 2018 quan dos incendis forestals es van iniciar i els mitjans humans no van ser capaços d’extingir-los fins al cap d’uns dies quan ja havien mort més de 100 persones.

![map](_static/loc_kineta.jpg)

Els mesos de canícula estival mediterrània (que vol dir que la línia de temperatures d’un climograma es troben per sobre de les barres de precipitació) van propiciar que una guspira iniciada als cables d’electricitat es convertís en un dels incendis forestals més mortífers d’Europa. Les urbanitzacions disperses enmig del bosc van agreujar la situació i van fer que les flames s’estenguessin pel terreny fins assolir les cases, on moltes persones i animals hi van quedar atrapats.

Les imatges obtingudes des de l’aire no permeten identificar clarament els boscos cremats dels no cremats, però gràcies als sensors que els satèl·lits duen incorporats, es pot diferenciar clarament quines extensions de terreny han estat cremades de les que no:

![split](_static/foc_kineta_compare.gif)

La combinació de bandes 12-8-4 del satèl·lit Sentinel 2 permet diferenciar perfectament entre les zones cremades (en vermell) i les zones no cremades (en verd):

![timelapse](_static/foc_kineta.gif) ![timelapse](_static/foc_neamakri.gif)

A l’esquerra el foc de Kineta entre el 20 de juliol i el 9 d’agost. A la dreta el foc de Nea Makri entre el 20 de juliol i el 19 d’agost.

El tractament d’imatges satèl·lit permet quantificar amb exactitud les hectàrees cremades després dels incendis forestals.

## Foc de la Ribera d’Ebre, Catalunya ##

També n’és un bon exemple el foc que el 26 juny de 2019 es va iniciar al terme municipal de la Torre de l’Espanyol i que va assetjar les Terres de l’Ebre deixant centenars de caps de bestiar calcinats i més de 5.000 ha de terreny agroforestal cremades.

![map](_static/loc_riberaebre.jpg)

També en aquest cas és clau la combinació de bandes del satèl·lit Sentinel 2 ja que amb la combinació de bandes de color natural (4-3-2) és pràcticament indistingible el terreny cremat de la resta.

![split](_static/foc_riberaebre_compare.gif)

La disponibilitat d’imatges sense núvols determina en quina mesura disposem d’imatges que ens permetin estudiar l’evolució des de l’inici del foc fins el final, passant pels trams intermedis. En el cas de la Ribera d’Ebre la falta d’imatges entre els dies 26 de juliol (data d’inici de l’incendi) i el 30 (moment en què es dóna per extingit) fa que el *timelapse* només copci el moment previ i el posterior a la catàstrofe.

![timelapse](_static/foc_riberaebre.gif)

## Focs de l’Amazònia, Sud Amèrica ##

Els focs que van tenir lloc simultàniament a multitud de punts de la selva amazònica durant els mesos d’agost i setembre de 2019 van ser el punt de mira del planeta sencer durant setmanes.

![map](_static/loc_amazonia.jpg)

L’augment de les temperatures a escala planetaria afavoreixen la proliferació dels incendis, però la principal amenaça en aquest cas és la intencionada crema de boscos. El cas és que les polítiques del president brasiler Jair Bolsonaro han estat criticades arreu ja que promouen la implementació de grans extensions de monocultius a costa de calcinar centenars de milers d'hectàrees de selva equatorial.

![timelapse](_static/foc_selvaamazonica_actual.gif), ![timelapse](_static/foc_selvaamazonica_pluja.gif)

Els incendis forestals de l’Amazònia es propaguen simultàniament en diversos punts i són pràcticament imparables per mitjans humans. Ni els avions ni els equips de bombers en poden frenar el seu desenvolupament. Les imatges animades anteriors mostren com ni tan sols les pluges són capaces d’aturar el foc.

![foc selva amazonica](_static/truecolor_amazones.jpg)

A simple vista no es pot diferenciar l’extensió cremada i és per això que necessitem la combinació de bandes i l’aplicació d’alguns algoritmes entre bandes que ressalten certs aspectes del territori com la vegetació, la humitat o el foc.

Algunes combinacions de bandes permeten visualitzar clarament l’extensió cremada de la resta, mentre que altres permeten, fins i tot, detectar l’avenç de la llengua del foc. A continuació es mostren alguns exemples on es visualitza clarament les àrees cremades de la selva sense cremar:

![NDVI foc](_static/ndvi_amazones.jpg)

L’índex NDVI ( Normalized Difference Vegetation Index) combina les bandes 4 i 8 que són relatives a la llum de color vermell i l’infraroig proper, respectivament. En concret l’NDVI es calcula així: (B8-B4)/(B8+B4) el que significa que mesura la diferència entre l’infraroig proper (B8), ones reflexades per la vegetació, i la llum vermella (B4), que és absorbida per la vegetació. D’aquesta manera s’obté un índex amb valors que oscilen entre -1 i +1, on els valors negatius corresponen a masses d’aigua i els valors propers a 1 corresponen a la coberta vegetal. Els valors positius propers a 0 corresponen a les zones cremades.

![foc selva amazonica](_static/falscolorurba_amazones.jpg)

Per detectar les flames o la llengua d’avanç del foc recorrem a les ones encarregades de captar la temperatura, els rajos infrarojos. La combinació de bandes dels rajos infrarojos de longitud d’ona curta (SWIR o Short-wave infrared) amb la llum vermella (12, 11, 4) ens proporcionen unes imatges que permeten identificar la presència del foc en el precís instant en què es va capturar la imatge.

![foc selva amazonica](_static/swir_amazones.jpg)

En canvi, si a més de la llengua de foc volem identificar les zones cremades en una sola imatge podem combinar els rajos infrarojos de longitud d’ona curta (que ens identifiquen el foc), amb l’infraroig proper (NIR o Near infrared) i la llum vermella (12, 8, 4), que ens identifiquen la vegetació.

## Foc de Gran Canaria, Illes Canàries ##

El foc de Gran Canaria es va iniciar el 17 d’agost de 2019 al poble de Vallesco i va devastar al voltant de 10.000 hectàrees de terreny durant els tres dies que va romandre actiu. L’incendi va cremar bona part del paratge de  Risco Caído i de la Montaña Sagrada, indrets declarats patrimoni de la humanitat per la UNESCO just un mes abans del desastre.

![map](_static/loc_canaria.jpg)

Les altes temperatures, el vent i la vegetació seca van ser claus perquè la velocitat d'avanç del foc fos superior a la capacitat dels efectius de bombers per extingir-lo.

En aquesta ocasió, la combinació de bandes de penetració atmosfèrica és clau per a visualitzar les zones cremades. En concret, es combinen les bandes dels rajos infrarojos de longitud d’ona curta (SWIR) i l’infraroig proper (Narrow NIR): 12-11-8a. Amb aquesta combinació de bandes s’aconsegueix que la vegetació més frondosa aparegui en tonalitats blaves (més fosc contra més frondós és la vegetació), mentre que la vegetació seca o morta apareix en tonalitats marronoses (més fosques contra més seca o calcinada està la vegetació).

![canaries](_static/Gran-Canaria-fire-timelapse.gif)

Com s’ha mostrat anteriorment, l’índex NDVI també és una bona opció per a mostrar les àrees afectades per un incendi. Aquesta combinació de bandes mostra en colors verdosos la vegetació (tons més foscos contra més frundosa sigui la vegetació) i tons marronosos (que esdevenen groguencs o marró crema si els terrenys estan completament desprovistos de vegetació). D’aquesta manera, la següent imatge mostra com les zones cremades passen de tenir un color verdós a tenir-ne un de groguenc.

![canaries](_static/Gran-Canaria-NDVI-timelapse.gif)

###### Atlas d’incendis forestals
###### L’Agència Espacial Europea (ESA - European Space Agency) ha processat les imatges satèl·lit a diari per detectar-hi els incendis forestals. Els resultats obtinguts han estat publicats en forma d’atles d’incendis forestals (https://s3worldfireatlas.esa.int/gallery / http://due.esrin.esa.int/page_wfa.php). Per exemple, la següent imatge animada mostra els incendis forestals a Europa des del 15 al 30 de juny de 2019, si us fixeu amb atenció, l’incendi de la Ribera d’Ebre queda registrat en aquest recull.

![map](_static/atles.gif)

# Retroces de glacials

L’escalfament global està originant el desgel dels pols i també el retrocés de les glaceres de muntanya. El processament d'imatges satèl·lit permeten analitzar l’evolució de les masses de gel i quantificar-ne el retrocés.

**El glacial de Vatnajökull, Islàndia**

El glacial de Vatnajökull és el camp de gel més voluminós d’Europa i el segon en superfície després de la glacera d’Austfonna, a Noruega. A causa de l’activitat volcànica i de l’escalfament global, el glacial ha sofert una notable regressió al llarg de les darreres dècades.

![map](_static/loc_islandia.jpg)

Les imatges aèries no permeten diferenciar entre les masses de gel i els núvols, és per això que combinem les bandes dels rajos infrarojos de longitud d’ona curta (11 i 12) i la banda que capta el vapor d’aigua (la 9): 11-12-9. Aquesta combinació de bandes destaca les masses de gel, que les pinta en color blau, i els núvols, que els pinta de color blanc grogós. En canvi, la resta de la superfície terrestre està representada per colors lilosos (corresponents a la vegetació) i colors verdosos (que corresponen a masses d’aigua, terrenys pantanosos o fondalades molt humides).

![vatnajokull](_static/islandia2.jpg)

![vatnajokull](_static/islandia1.jpg)

La combinación de bandes 3-11-8 també ens permet identificar masses de gel, que són molt difícils de diferenciar a simple vista per mitjà d’imatges satèl·lit sense processar. Aquesta vegada, les masses de gel queden pintades de color rosa fucsia, el que les permet distingir de la resta del terreny representat en tons blavosos (més fosc contra més vegetació). L’exemple que segueix a continuació, és una imatge de l’Aneto entre 2015 i 2019.

![aneto](_static/aneto_retroces.gif)

Aquesta combinació de bandes ens permet inlcús localitzar les petites congestes que a l’estiu resten sense desfer a les capçaleres de les muntanyes pirinenques:

![pirineu](_static/pirineu_congestes.gif)

## Barra de gel d’Amery (Iceberg D28), Antàrtida ##

L’Antàrtida és un continent situat al sud del globus terraqui que té tota la superfície terrestre coberta de neu i gel. Les glaceres arriben fins el mar i avancen com una llengua de gel oceà endins. De tant en tant, el front de les glaceres es desfà per la part inferior, la que toca al mar, i bona part de la llengua de glaç queda suspesa fins que colapsa i es desprèn, originant així els icebergs. Aquest fenomen està desvinculat del canvi climàtic i succeeix de forma cíclica i natural com a part del cicle de vida de les glaceres. A la zona d’Amery aquest procés es dóna cada 60 - 70 anys i recentment hem assistit al darrer dels episodis.

![map](_static/loc_amery.jpg)

A finals de setembre de 2019 un iceberg d’uns 1.600 metres quadrats es va desprendre de la barra de gel d’Amery, una plataforma de gel que es recolza sobre l’oceà Antàrtic i que avança mar enllà. La darrera vegada que aquest procés va succeir va ser entre l’any 1963 i des d’aleshores els científics guaitaven aspectants el següent esdeveniment. Aquesta vegada l’immens iceberg ha ocupat una superfície equivalent a la ciutat de Londres i se n’ha pogut monitoritzar el seu seguiment gràcies als satèl·lits que orbiten la Terra.

L’animació que segueix a continuació s’ha realitzat amb les imatges del satèl·lit Sentinel 1, un aparell que orbita la terra i que allotja sensors actius. Això implica que es pot obtenir informació del seu radar de manera regular, independentment de les condicions atmosfèriques.

![iceberg](_static/amery_S1.gif)

# Inundacions

Les inundacions són una catàstrofe natural que amenaça alguns pobles i ciutats situats prop de les lleres o les desembocadures dels rius. L’increment en la freqüència i la intensitat dels temporals, a causa del canvi climàtic, agreuja el problema que, en origen, es remet al planejament urbanístic vigent.

El creixement urbà de les zones poblades ha incrementat exponencialment en les darreres dècades, moment en què l'ordenament del territori era lax o inexistent. Això ha implicat que bona part de l’estructura urbana se situa dins de la llera del riu, ja sigui dins del període de retorn de 50, 100 o 500 anys.

Per entendre això, primer cal tenir en compte que els rius tenen avingudes extraordinàries que causen un sobreeiximent del flux d'aigua respecte el seu cabal ordinari. Contra més gran sigui el període de retorn, més virulència té l’avinguda. Per exemple, les avingudes amb període de retorn de 100 anys fan referència a que cada any hi ha un 1% de possibilitats que hi hagi una gran avinguda; mentre que els períodes de retorn de 50 anys equivalen a un 2% de possibilitats anuals que el riu sobrepassi els seus límits ordinaris, i els període de retorn de 500 anys impliquen que cada any hi ha un 0.2% de possibilitats que hi hagi una crescuda molt extraordinària del riu.

Com que el planejament és inferior a aquests 50, 100 i 500 anys, sovint assistim a crescudes dels rius, els efectes de les quals tenen conseqüències devastadores per a pobles i ciutats que mai havien vist créixer tant el riu. En general, això no és conseqüència del canvi climàtic, sinó que les cases estan emplaçades en el “territori del riu”. Si quan plou molt un riu sobresurt del seu cabal, segurament fa 50, 100 o 500 anys ja ho havia fet, però les conseqüències no van ser devastadores perquè llavors no hi havia cases ni carreteres. Per això es diu que “el riu té memòria”.

![map](_static/espluga.jpg)

Aquest procés es veu agreujat per una altra dinàmica territorial que s’ha accelerat a partir de les últimes dècades. Es tracta del despoblament o èxode rural que ha comportat l'abandonament de les activitats tradicionals i, per tant, de la gestió dels boscos. El pasturatge, la silvicultura i el carboneig s’encarregaven d'extreure arbres i matolls del sotabosc, fet que convertia els terrenys forestals en mosaics agroforestals. Això implica que la quantitat de bosc de fa anys era menor i el boscos que hi havia estava gestionat: sense sotabosc i amb arbres ferms i sans. En canvi, els boscos d’avui s'estenen per valls i muntanyes formant un continuo de massa forestal densa en tots els seus estrats (arbori i arbustiu) i composta tant per arbres sans com per arbres i arbustos morts.

És en aquest context de boscos densos i amb biomassa morta quan els episodis de pluges abundants són especialment perilloses. L’aigua d’escorrentia neteja els vessants de matèria orgànica morta que arrossega fins el riu. Els cursos fluvials sobresurten de la seva llera ordinària i s’enduen al seu pas troncs d’arbres. Els arbres són arrossegats aigües avall del riu fins que topen amb un obstacle que els impedeix el pas. Sovint, aquests obstacles són infraestructures artificials, com els arcs del pont de la imatge de sobre. Aquest embossament impedeix que l’aigua segueixi el seu curs i l’obliga a agafar una trajectòria al voltant del pont taponat. Això agreuja les inundacions que, sense taponaments, ja serien catastròfiques de per sí.

## Gota freda Elche, Alacant ##

El dia 12 de setembre de 2019 un episodi de pluja torrencial va irrompre la conca hidrogràfica del riu Segura. Aquest curs fluvial es va desbordar en el seu darrer tram, molt a prop de la Gola del Segura, quan el passat setembre els aiguats van superar els 42 l/m2 i el riu va ser incapaç de drenar tanta quantitat d’aigua. La comarca del Baix Segura és una planura quasi sense pendent gràcies a la gran quantitat de sediments fluvials i a l’explotació agrícola del terreny durant centenars d’anys.

![map](_static/loc_baixsegura.jpg)

Les imatges satèl·lit comparen la plana del Segura abans (24 d’agost) i després de les inundacions (18 de setembre) i revelen com les inundacions han ocupat milers d’hectàrees de camps de cultiu. Fins i tot, les imatges aèries permeten observar la gran quantitat de sediments que ha aportat aquesta riuada al mar.

![floods](_static/GotaFreda_split.gif)

Malgrat que les imatges en color real permeten veure les inundacions, l’índex NDWI (Normalized Difference Water Index) combina les bandes 3 i 8 (corresponents a la llum verda i a l’infraroig proper) per diferenciar les masses d’aigua (en blau) de la superfície terrestre (en verd, més fosc contra més humitat conté la vegetació).

![floods](_static/NDWI_gotafreda_timelapse.gif)

## Temporal Glòria ##

El temporal Gloria va tenir lloc entre els dies 20 i 23 de gener de 2020 a la península Ibèrica i la Catalunya Nord amb pluges molt intenses al litoral mediterrani i nevades importants a l'Aragó.

![map](_static/loc_gloria.png)

Va causar múltiples destrosses i incidències a bona part del territori i, fins i tot, va ocasionar tretze víctimes mortals. Aquesta borrasca va ser peculiar perquè va incrementar les precipitacions i va ocasionar un temporal de llevant. Això va comportar un increment del cabal del riu que es va sumar a l’impacte de les llevantades al litoral, afectant les zones properes als rius i tots els municipis del litoral.

El temporal va tenir especial afectació al Delta de l'Ebre, on les onades van ultrapassar els 14 metres d'alçada i l'aigua de la mar va arribar a penetrar fins a tres quilòmetres endins, negant fins a 3.000 hectàrees de camps d'arròs.

![floods](_static/sentinel1_delta.gif)

El programa Copernicus de l'ESA compta amb diferents satèl·lits que actualment orbiten la terra i registren imatges a temps real. El satèl·lit Sentinel-1 porta incorporat un radar que permet la captura de dades malgrat les condicions atmosfèriques i malgrat ser de nit. Per aquest motiu, es van poder capturar imatge en el mateix moment que tenia lloc el temporal.

A la imatge procedent del satèl·lit Sentinel-1 s'observa en tons blaus els cossos d'aigua mentre que en tons verds i marrons es mostra la terra. Les aigües es mostren d'un color blau clar si estan mogudes, mentre que es mostren en colors foscos si estan en calma. Per això hi ha diferència de tonalitats entre ambdues imatges.

La comparació d'aquestes imatges mostra l'abans i el després del temporal Glòria i s'observa com, efectivament, el delta ha quedat parcialment inundat. Hem de pensar que durant el temporal al delta ha rebut aigua procedent del riu, de la precipitació i de el temporal de llevant. No es diferencia entre les aigües dolces i salades per la qual cosa amb la imatge és impossible de distingir l’aigua procedent del mar de la procedent del desbordament del riu.

Al marge del detal de l'Ebre, aquest temporal va tenir afectació sobre diversos rius que van desbordar-se, com és el cas del Tec, el Tet, l'Aglí, el Ter, el Fluvià o la Tordera. Es van haver de desallotjar diversos habitatges i els ciutadans del baix Ter es van haver de confinar. Altres rius van tenir un cabal al límit del desbordament, però sense arribar a fer-ho, com és el cas de l'Onyar al seu pas per la ciutat de Girona.

Les imatges del radar que duu incorporat el satèl·lit Sentinel 1 també va permetre visualitzar les inundacions de les planes de l'Alt i el Baix Empordà:

![floods](_static/roses.gif)

![floods](_static/pals.gif)

En la desembocadura de la Tordera es van inundar 807 hectàrees, afectant Blanes, Tordera, Malgrat de Mar i Palafolls. Bona part de les restes de matèria orgànica i plàstics aportats per la Tordera es van acumular al nord del Port d’Arenys, a la platja del Cavalló, fruit de la deriva litoral. Milers de voluntaris es van mobilitzar per a retirar les restes de canya americana, troncs i plàstics que l’onatge havia confinat a la platja.

![floods](_static/cavallo.jpg)


# Erupcions volcaniques

Entre els fenòmens naturals que es poden detectar amb la imatge satèl·lit, les erupcions volcàniques són especialment curioses de d’observar.

Per detectar la lava recorrem a les ones encarregades de captar la temperatura, els rajos infrarojos. La combinació de bandes dels rajos infrarojos de longitud d’ona curta (SWIR o Short-wave infrared) amb la llum vermella (12, 11, 4) ens proporcionen unes imatges que permeten identificar la presència del foc en el moment en què es va copsar la imatge.

## Volcà Stromboli i Etna ##

Els volcans Stromboli i Etna, situats a Itàlia, tenen activitat volcànica constant i gràcies a la combinació de bandes de les imatge satèl·lit es pot observar com els con volcànic emeten fum i colades de lava.

![volcano](_static/Etna_timelapse.gif)  ![volcano](_static/Stromboli_timelapse.gif)

## Volcà Kilauea, Hawaii ##

El volcà Kilauea va entrar en activitat el dia 3 de maig de 2018 i va estar actiu fins el 15 d’agost. L’activitat volcànica va anar seguida d’una sèrie de terratrèmols que, al seu torn, van incrementar encara més les erupcions. La colada de lava avançava ràpidament i les fonts de lava sobrepassaven els 90 metres d’alçada.

![map](_static/loc_hawai.jpg)

La colada de lava va arribar al mar el 4 de juny i de camí va destruir un complex residencial sencer, el Vacationaland. La lava també va evaporar l’aigua del llac més gran de Hawaii, el llac Verd situat al cràter del volcà de Kapoho. El 7 d’agost la colada de lava ocupava 35 km2 de superfície i havia guanyat més de 3.5 km2 de terra al mar.

![radar](_static/Hawaii-landgrow-timelapse.gif) ![hawaii](_static/Hawaii-timelapse.gif)

L’erupció volcànica es pot visualitzar perfectament amb la mateixa combinació de bandes que hem utilitzat per a detectar la llengua d’avanç dels incendis forestals: 11-12-4 (SWIR2, SWIR1 i llum vermella). Malgrat la presència de fum, cendres i gas que emet l’activitat volcànica la imatge permet identificar clarament la colada de lava.

L’animació de la dreta, en canvi, mostra imatges recollides amb el satèl·lit Sentinel 1, les quals han permès determinar amb precisió l’avenç de la lava cap al mar i quantificar la quantitat de terra de nova formació.

# Desforestacio

Com hem vist en apartats anteriors, la proliferació dels boscos en països desenvolupats agreuja els problemes d’incendis forestals i d’inundacions. En canvi, en països en vies de desenvolupament el procés és invers: la tala massiva de boscos per l’aprofitament de la fusta o per a la implementació de grans extensions de monocultius són una de les principals causes de pèrdua de biodiversitat. La desforestació redueix i fragmenta els hàbitats, i també amenaça en extingir espècies i comunitats locals vinculades en aquests hàbitats o entorns.

## San José de Chiquitos, Bolivia ##

Les animacions següents mostren la desforestació a l’entorn de San José de Chiquitos des d’agost de 2015 fins novembre de 2019.

![map](_static/loc_sanjose.jpg)

En aquest cas, les imatges en color natural (4-3-2) són suficients per veure la magnitud del problema. La majoria de parcel·les telades esdevindran camps de monocultiu, sobretot de soja, canya de sucre i arròs, o bé camps de dall per a la producció de farratges o pastures per al bestiar boví.

![deforestacio](_static/deforestation-bolivia-timelapse.gif) ![deforestacio](_static/deforestation-bolivia2-timelapse.gif)

# Sequeres

Les sequeres cada vegada són més freqüents i més severes, sobretot en ambients que de per sí ja són àrids.

## Llacuna d’Aculeo, Xile ##

La llacuna d’Acuelo era una destinació pel turisme nacional de Xile fins que, des de fa ben poc, ha començat a desaparèixer del mapa.

![map](_static/loc_aculeo.jpg)

El 2011 la llacuna tenia 12 km2 i arribava fins els 6 metres de profunditat. Entres les causes de desaparició hi ha el canvi climàtic ja que des de 2010 no hi plou a la zona, però també la utilització de l’aigua pel cultiu d'alvocat i per l’abastament de la ciutat de Paine. La imatge animada de sota mostra el ràpid assecament de la Llacuna des de setembre de 2016 fins octubre de 2019.

![aculeo](_static/aculeo-timelapse.gif)

## Dessecació del mar d’Aral, Uzbekistan/Kazakhstan (21.6.2016 - 19.10.2019) ##

Però per sobre de totes les sequeres, destaca el desastre natural derivat de la mala gestió dels recursos hídrics que es va dur a terme al mar d’Aral.

![map](_static/loc_aral.jpg)

Aquest mar interior havia arribat a ser el quart llac més gran del món amb una extensió de 68 km2. Des de 1960 s’ha anat reduint després que els rius que l’alimentaven van ser objecte d’obres hidràuliques per a la irrigació de terres de cultiu a través de projectes soviètics. En concret, l’aigua dels rius Syr Darya i Amu Darya va ser desviada i canalizada per a convertir el desert en camps de cultius, principalment de cotó, de més de 7.5 milions d’hectàrees.
![aral](_static/AralSea1989_2014.jpg)

A més de l’impacte paisatgístic que suposa la dessecació del mar d’Aral, també hi ha conseqüències ambientals que han comportat un greu problema de salut pública a la zona. L’ús de pesticides i fertilitzants ha contaminat les aigües d’escorrentia que s’utilitzen per a abastir la població local. L’alta contaminació de l’aigua per l’ús domèstic ha suposat un increment d’enfermetats com anèmia, tiroides i càncer de diferents tipus.

Al marge de les enfermetats associades a la contaminació, les comunitats locals, que basaven la seva economia i subsistència en l’activitat pesquera, s’han vist fortament afectades per la desaparició del mar. El retrocés accelerat del mar ha deixat abandonats desenes de vaixells, testimonis d’una activitat que des de fa dècades ha quedat extingida. Actualment, la ribera del mar queda a desenes de quilòmetres dels pobles que antigament se situaven al litoral.

Les imatges satèl·lit permeten veure la tendència a desaparèixer del mar d’Aral entre 2017 i 2019.

![aral](_static/Lake-Aral-timelapse.gif)

# Urbanitzacio

El processament d’imatges satèl·lit també permet quantificar i fer un seguiment evolutiu de l’ocupació accelerada del sòl.

## Aeroport de Beijing, Xina ##

L’aeroport internacional de Beijing Daxing és el més gran del món i ha estat construït entre els anys 2014 i 2019.

![map](_static/loc_beijing.jpg)

L’animació següent mostra com les terres de cultiu de les afores de Beijing s’han convertit en pistes d’aterratge i enlairament, situades al voltant de l’aeroport que dibuixa una gran estrella visible des de l’espai.

![beijing](_static/beijingairport.gif)


Recurs didàctic: Story map 'Study cases: how to identify environmental changes with satellite imagery?': https://unigis-girona.maps.arcgis.com/apps/Cascade/index.html?appid=7e837a23b4874c1789548a06284ee9ed
