# INTRODUCCIÓ: QUÈ ÉS LA TELEDETECCIÓ?

Les dinàmiques socials, econòmiques i territorials que la humanitat ha adoptat a partir de la Primera Revolució Industrial han comportat un consum indiscriminat de recursos naturals. L’explotació d’aquests recursos ha compromès severament el benestar dels habitants del planeta terra, així com dels sistemes físics que el sustenten. Aquest procés s’ha descrit com a **canvi ambiental global** i implica tres fenòmens ben coneguts: el canvi climàtic, la pèrdua de biodiversitat i el canvis d’usos i cobertes del sòl. Tot plegat implica la degradació del medi ambient que suposa una amenaça per a les plantes, els animals i les persones del planeta.

La disponibilitat d’imatges satèl·lit arreu del món amb una freqüència que pot arribar a ser diària (si les condicions atmosfèriques ho permeten), facilita la identificació i el seguiment de tots aquests fenòmens naturals i processos antròpics que impliquen canvis notables a la superfície terrestre. La fotografia aèria presa des de l’espai permet estudiar l’evolució i detectar les conseqüències de les erupcions volcàniques, els terratrèmols, els incendis forestals, les inundacions, el desglaç de glaceres, la desforestació o la urbanització. Però com s’obtenen i es processen les imatges de la terra és una qüestió tan fascinant que es mereix una explicació detallada sobre els satèl·lits i els sensors que les capturen, així com les potencialitats que ofereixen.

La ciència que s’ocupa d'obtenir informació sobre la superfície terrestre sense estar-hi en contacte s’anomena **teledetecció**. Això és possible gràcies als sensors que compilen imatges des de plataformes o aparells que volen per l’espai aeri, com els avions o els drons, o en plataformes espacials, com els satèl·lits. El principi bàsic de la teledetecció és capturar la radiació electromagnètica que reflecteixen els objectes a partir de sensors. 

Un **sensor** és l’aparell que permet registrar una imatge, ja sigui amb una càmera fotogràfica convencional que recull i combina tres bandes dins la regió de llum visible de l’espectre electromagnètic; o amb un sensor més especialitzat capaç de compilar centenars de bandes repartides al llarg d’una regió molt àmplia de l'espectre electromagnètic. En paraules més tècniques, un **sensor** és un element que té la capacitat de llegir la radiació electromagnètica d’un objecte dins una zona concreta de l’espectre i registrar-ne la seva intensitat. Cada lectura i registre que fa un sensor al llarg de l’espectre electromagnètic s’anomena **banda**. 

La **radiació electromagnètica** és el conjunt d’ones electromagnètiques que es propaguen a l’espai. Els objectes que emeten aquestes ones estan subjectes a una font d’energia (generalment procedent de la llum solar) que és la responsable d’aquesta emissió. Els sensors, doncs, capturen la radiació que emeten els objectes després d’haver estat sotmesos a una font d’energia. Hi ha dos tipus de sensors: els passius, que registren la radiació solar que els objectes reflecteixen; i els actius, que apliquen una radiació als objectes i en mesuren la seva reflexió.

Els **sensors passius** (esquerre de la imatge) depenen de la presència de la llum solar i només se’n poden obtenir imatges si les condicions atmosfèriques permeten l'observació de la Terra. En canvi, els **sensors actius** (dreta de la imatge) permeten obtenir imatges malgrat la presència de núvols o malgrat ser de nit. Aquests sensors emeten radiació cap a la superfície terrestre el reflex de la qual és enregistrat pel sensor.

![sensors passius](_static/passive.JPG)   ![sensors actius](_static/active.JPG)    

La radiació electromagnètica que capturen els sensors són ones de diferents longituds i freqüències. La longitud d’ona és la distància que hi ha entre dues crestes successives, mentre que la freqüència fa referència al nombre de crestes que passen per un mateix punt durant un període de temps. Aquestes dues variables estan inversament relacionades, és a dir, contra més curta és la longitud d’ona, més gran és la freqüència; i contra més llarga és la longitud d’ona, més baixa és la freqüència.
**L’espectre electromagnètic** comprèn des de les ones gamma, de longituds d’ona curta, fins a ones de radio, de longituds d’ona llarga. Entre aquestes dues ones trobem una amalgama d’ones com els rajos ultravioletes, els rajos X, els rajos infrarojos, o la llum visible. 

L’ull humà només captura el rang de l’espectre electromagnètic que oscila entre **0.4** i **0.7 μm**, és a dir, la llum visible. Més enllà d’aquesta longitud d’ona se situen els rajos ultraviolats, a l'esquerra de la llum visible, i els rajos infrarojos, a la dreta. Per detectar el reflexe dels objectes en aquestes longituds d’ona necessitem sensors especialitzats que percebin més enllà de les cèl·lules sensores de l’ull humà. Actualment, els sensors remots capturen imatges des dels rajos ultraviolats fins als rajos infrarojos; més enllà d’aquestes longituds d’ona, els sensors remots no registren imatges. 

![L’espectre electromagnètic](_static/espectreelectromagnetic.jpg) 

Cada objecte té una **signatura espectral** o percentatge de reflectancia en cada regió de l’espectre electromagnètic. Segons la manera que cada objecte reflecteix l’energia del sol, es pot identificar en les imatges satèl·lit i elaborar, per exemple, mapes de vegetació o identificar masses d’aigua dins d’un continent.

![signatura espectral](_static/signaturaespectral.jpg) 

Com hem comentat anteriorment, cada registre que fa un sensor al llarg de l’espectre electromagnètic s’anomena **banda**. Aquestes bandes són imatges que tenen un valor determinat a cadascun dels píxels segons la reflexió de l’objecte. Per visualitzar una imatge en color s’ha de combinar més d’una banda. Però abans d’entendre com funciona la combinació de bandes, cal fer un petit incís a la composició dels colors.

La composició de color es pot dur a terme mitjançant dos processos, el procés substractiu (que es duu a terme en les arts plàstiques), i el procés additiu (que correspon a la visualització digital mitjançant sistemes electrònics).  

En els processos substractius (dreta de la imatge), el color és el resultat de l’absorció de la llum dels respectius colors complementaris. En les arts plàstiques els colors primaris són el magenta, el groc i el cian que si es barregen tots s’obté el negre (CMYK). En canvi, en els processos additius (esquerre de la imatge), el color és el resultat de la combinació dels tres colors primaris, el vermell, el verd i el blau (R, G, B). Aquesta combinació de colors es fa servir quan la imatge es representa en una pantalla digital que emet i combina la llum vermella, verda i blava. El color de cada píxel depèn de la intensitat de llum vermella, verda i blava de cada píxel.

![RGB_CMYK](_static/RGBCMYK.JPG) 

Les imatges del satèl·lit capturen les bandes corresponents a la llum vermella, verda i blava. En realitat, les bandes de llum vermella, verda i blava són imatges en blanc i negre on cada píxel té un valor determinat corresponent a la proporció de llum vermella, verda o blava que té cada objecte.

![RGB](_static/rgb.JPG) 

Si les bandes de llum vermella, verda i blava es filtren dins dels canals o rajos de llum vermell, verd i blau s’obten tres imatges, una vermella, una verda i l’altra blava que si es combinen s’obté una imatge en color real, tal i com veiem els objectes amb els nostres ulls. 

![true colour](_static/truecolor.jpg) 

Al marge de la llum vermella, verda i blava, els satèl·lits també capturen altres bandes corresponents a diferents porcions de l’espectre electromagnètic. Aquestes bandes també són imatges en blanc i negre que tenen un valor de píxel determinat i que si es combinen dins dels canals RGB del monitor del nostre ordinador, es poden obtenir imatges ben curioses i interessants. Per exemple, podem obtenir un fals color per detectar els incendis forestals combinant les bandes dels rajos infrarojos de longitud d’ona curta (SWIR o Short-wave infrared) al canal vermell, la banda d’infraroig proper (NIR o Near infrared) al canal verd i la banda de llum vermella al canal blau (12-8-4, segons les bandes de Sentinel-2):

![false colour](_static/falsecolor.jpg) 

Els satèl·lits que capturen les bandes són objectes que orbiten la Terra amb l’objectiu de registrar imatges de la superfície terrestre de manera periòdica. Cada agència espacial té els seus propis satèl·lits, de manera que l’òrbita del planeta està recoberta per una quantitat sorprenent de ferralla. 

![space](_static/space.JPG) 

La **NASA** (National Aeronautics and Space Administration) captura les imatges de la Terra mitjançant una sèrie de satèl·lits que es diuen **Landsat**. En canvi, l’Agència Espacial Europea (**ESA** - European Space Agency) dins del programa **Copernicus** ha desenvolupat un conjunt de missions per deixar en òrbita els seus propis satèl·lits que s’anomenen **Sentinels**. 
De moment, s’han dut a terme 7 missions Sentinel basades en alliberar plataformes o satèl·lits que duen incorporats diferents sensors com radars i multiespectrals, així com instruments per a monitoritzar l’oceà i l'atmosfera. El conjunt de sensors capturen les diferents bandes que posteriorment es poden tractar i combinar per obtenir informació sobre la superfície terrestre. Actualment hi ha 4 Sentinels en òrbita:

- **Sentinel 1** és un satèl·lit que orbita la Terra des dels pols i registra imatges dia i nit independentment del temps atmosfèric gràcies a un radar.
- **Sentinel 2** també orbita la Terra des dels pols i duu incorporat un sensor multiespectral d’alta resolució que permet capturar imatges per monitoritzar la vegetació, el sòl, les masses d’aigua i les zones costaneres.
- **Sentinel 3** és una plataforma que incorpora multitud d’instruments per mesurar la topografia de la superfície del mar, la temperatura superficial terrestres i de l’oceà.
- **Sentinel 5P (precursor)** pretén substituir el satèl·lit Envisat que es va extraviar el 2012. Al seu torn, aquesta plataforma serà substituida pel Sentinel 5 que s’enviarà el 2021. Els Sentinel 5 proporcionen dades atmosfèriques que pretenen monitoritzar la qualitat de l’aire, sobretot en relació als contaminants com l’ozò, el diòxid de nitrogen i el diòxid de sofre. 

Futures missions:

- **Sentinel 4** és un satèl·lit Meteosat Tercera Generació que proporcionarà dades sobre la composició atmosfèrica.
- **Sentinel 5** també proporcionarà dades sobre la composició atmosfèrica i s’embarcarà en una nau espacial EUMETSAT Polar System (EPS).
- **Sentinel 6** pretén mantenir les missions del satèlit Jason-2 sobre càlculs altimètrics d’alta precisió tant de la superfície terrestre com de l’oceà.

![sentinels](_static/sentinels.JPG) 


Recurs didàctic: Story map 'What is remote sensing': https://unigis-girona.maps.arcgis.com/apps/Cascade/index.html?appid=9856ae0d9dc54d20ab2c4c0f6f6ec8f1  
