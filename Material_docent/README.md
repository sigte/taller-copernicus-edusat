# Material docent 


## Descripció:
L’activitat consta de dues parts que es poden allargar més o menys en funció del temps disponible i del perfil dels estudiants:

- Explicació teòrica sobre què és el canvi ambiental global i quines conseqüències té; i què és la teledetecció (els satèl·lits, els sensors i la combinació de bandes) i com es poden detectar els desastres naturals o accions antròpiques derivades del canvi ambiental global.

- Exercicis pràctics sobre la identificació de desastres naturals o accions antròpiques, com els incendis, les inundacions, les sequeres, la desforestació… a partir de l’EO Browser. 

## Durada:
El temps de l’activitat pot durar des d’uns pocs minuts fins a dues hores, en funció de la disponibilitat del grup.

- 15 - 30 minuts: breu explicació sobre què són els satèl·lits (sense entrar en detalls) i què permeten fer (identificar incendis, erupcions volcàniques, inundacions…).

- 2 - 4 hores: explicació detallada sobre els satèl·lit i els sensors i les seves potencialitats; mostrar els casos d'ús; ensenyar funcionament d’EO Browser i elaborar comparatives i *timelapses*

## Materials:

- Explicació teòrica sobre què és la Teledetecció?

- Explicació del casos d'estudi que mostren com podem utilizar les imatges satèl·lit per identifcar els efectes dels canvis territorials.

- Suport gràfic en forma de diapositives sobre l’explicació teòrica.
 
   - Story map 'What is remote sensing': https://unigis-girona.maps.arcgis.com/apps/Cascade/index.html?appid=9856ae0d9dc54d20ab2c4c0f6f6ec8f1  
   - Story map 'Study cases: how to identify environmental changes with satellite imagery?': https://unigis-girona.maps.arcgis.com/apps/Cascade/index.html?appid=7e837a23b4874c1789548a06284ee9ed

 - Manual d'ús de l'EO Browser.
 
 - Aplicació Sentinel Hub - EO Browser de SINERGISE*: https://apps.sentinel-hub.com/eo-browser/

###### *Es tracta d’una organització que no pertany directament al projecte Copernicus, però que n’és un dels principals actors en tant que consumeix i proporciona imatges dels satèl·lits Sentinel a partir d’un visor que permet visualitzar les imatges, fer combinacions de bandes i obtenir índexs. El producte resultant pot ser una simple imatge, una comparativa entre una o més imatges, o un timelapse.

## Recursos complementaris:

- https://www.sentinel-hub.com/explore/education/

- https://medium.com/sentinel-hub/remote-sensing-in-the-classroom-getting-started-guide-cb904392687e