# Activitat didàctica: Com utilitzar les imatges satèl·lit per a identificar els efectes dels canvis territorials?

Aquí trobareu diferents apartats:

- [Material docent](Material_docent) on es resumeixen les principals característiques de l’activitat didàctica.
- [Què és la Teledetecció?](Teledeteccio) on es donen les pautes i materials teòrics sobre Teledetecció per poder dur a terme l’activitat didàctica
- [Casos d'ús](Casos_us) on s'il·lustren diferentes exemples que mostren com podem utilizar les imatges satèl·lit per identifcar els efectes del canvi climàtic
- [Manual d'ús: EO Browser](Manual_us) on hi ha la informació teòrica i tècnica necessària per utilitzar l’EO Browser.


Aquesta activitat ha estat creada per el Servei de SIG i Teledetecció (SIGTE) de la Universitat de Girona.

© SIGTE - UdG